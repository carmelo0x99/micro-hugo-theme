---
title: "Snip #3"
date: 2022-06-06T16:01:34+02:00
publishdate: 2022-06-06
lastmod: 2022-06-06
draft: true
tags: ["test", "Linux"]
categories: ["howto"]
author: "Carmelo C."
---

“Il voler trattare le quistioni naturali senza geometria è un tentar di fare quello che è impossibile ad esser fatto.”

