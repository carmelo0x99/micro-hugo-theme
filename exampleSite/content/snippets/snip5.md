---
title: "Snip #5"
date: 2022-08-08T16:01:34+02:00
publishdate: 2022-08-08
lastmod: 2022-08-08
draft: true
tags: ["test", "Linux"]
categories: ["howto"]
author: "Carmelo C."
---

“Questi che esaltano tanto l'incorruttibilità, l'inalterabilità, etc. [dei corpi celesti], credo che si riduchino a dir queste cose per il desiderio grande di campare assai e per il terrore che hanno della morte: e non considerano che quando gli uomini fossero immortali, a loro non toccava a venire al mondo.”

