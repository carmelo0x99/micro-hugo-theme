---
title: "Snip #6"
date: 2022-08-08T16:01:34+02:00
publishdate: 2022-08-08
lastmod: 2022-08-08
draft: true
tags: ["test", "Linux"]
categories: ["howto"]
author: "Carmelo C."
---

“La differenza che è tra gli uomini e gli altri animali, per grandissima che ella sia, chi dicesse poter darsi poco dissimile tra gli stessi uomini, forse non parlerebbe fuor di ragione.”

