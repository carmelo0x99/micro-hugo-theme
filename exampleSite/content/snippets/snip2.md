---
title: "Snip #2"
date: 2022-05-05T16:01:34+02:00
publishdate: 2022-05-05
lastmod: 2022-05-05
draft: true
tags: ["test", "Linux"]
categories: ["howto"]
author: "Carmelo C."
---

“Infinita è la turba degli sciocchi, cioè di quelli che non sanno nulla; assai son quelli che sanno pochissimo di filosofia; pochi son quelli che ne sanno qualche piccola cosetta; pochissimi quelli che ne sanno qualche particella; un solo Dio è quello che la sa tutta.”

