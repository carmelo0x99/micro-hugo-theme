---
title: "Snip #4"
date: 2022-07-07T16:01:34+02:00
publishdate: 2022-07-07
lastmod: 2022-07-07
draft: true
tags: ["test", "Linux"]
categories: ["howto"]
author: "Carmelo C."
---

“Quello che noi ci immaginiamo bisogna che sia o una delle cose già vedute, o un composto di cose o di parti delle cose altra volta vedute; ché tali sono le sfingi, le sirene, le chimere, i centauri, etc.”

