---
title: "Snip #1"
date: 2022-04-04T15:59:34+02:00
publishdate: 2022-04-04
lastmod: 2022-04-04
draft: true
tags: ["test", "Linux"]
categories: ["howto"]
author: "Carmelo C."
---

“L'intelletto umano intende alcune cose così perfettamente, e ne ha così assoluta certezza, quanto se n'abbia l'istessa natura; e tali sono le scienze matematiche pure, cioè la geometria e l'aritmetica; delle quali l'intelletto divino ne sa bene infinite proposizioni di più, perché le sa tutte; da di quelle poche intese dall'intelletto umano...”

