---
title: "About"
date: 2023-05-24T12:32:51+02:00
draft: true
tags: ["about"]
categories: ["boring stuff"]
author: "Carmelo C."
---

# Micro Hugo Theme - exampleSite 
This static website has been made with [Hugo](https://gohugo.io/) and my custom theme, [Micro](https://gitlab.com/carmelo0x63/micro-hugo-theme).

<!--BaseURL can be used in Markdown as a shortcode as such: [BaseUrl]({{< baseUrl >}}).-->
[AbsURL](https://gohugo.io/functions/urls/absurl/) can be used in Markdown as a shortcode as such: [Home Page]({{< absUrl >}}).
