---
title: "Home Page"
date: 2023-01-24T16:37:34+02:00
publishdate: 2023-01-25
lastmod: 2023-01-26
draft: true
tags: ["homepage", "splash"]
categories: [""]
author: "Carmelo C."
---

# exampleSite starts here 
This text comes from `content/_index.md` (with an underscore `_`).

## Basic info 
[Markdown](https://www.markdownguide.org/) _tags_ are supported here...

### Some examples
```
# H1
## H2
### H3
```
# H1
## H2
### H3

```
1. list item #1
2. list item #2
3. list item #3
```
1. list item #1
2. list item #2
3. list item #3

```
Emphasis, aka italics: *asterisks* or _underscores_
Strong emphasis, aka bold: **asterisks** or __underscores__
Combined emphasis: **asterisks and _underscores_**
Strikethrough uses two tildes: ~~Scratch this~~
```
Emphasis, aka italics: *asterisks* or _underscores_

Strong emphasis, aka bold: **asterisks** or __underscores__

Combined emphasis: **asterisks and _underscores_**

Strikethrough uses two tildes: ~~Scratch this~~

```
| Tables~~~~~~~~ | Are also     | ~~~~~Supported  |
| -------------- |:------------:| ---------------:|
| this           | this         | this            |
| column is      | column is    | column is       |
| left-aligned   | centered     | right-aligned   |
```
| Tables~~~~~~~~ | Are also     | ~~~~~Supported  |
| -------------- |:------------:| ---------------:|
| this           | this         | this            |
| column is      | column is    | column is       |
| left-aligned   | centered     | right-aligned   |

---

The text below is merged from `layouts/index.html` (notice it's `*.html` this time).</br>
...3</br>
..2</br>
.1</br>
