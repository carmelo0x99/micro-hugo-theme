# Micro(scopic) Hugo Theme
Adhering to [xkcd's philosophy on 'Standards'](https://xkcd.com/927/), here's my attempt to create my own theme.<br/>
It's cute, tiny, minimalistic and all those things but, above all, it's mine to modify it however I fancy.<br/>
You can use it, of course. For free!<br/>
Here's how to do that.

### (Very) Quick run-through
1. clone or, better, **fork** the theme's repository:
```
$ git clone https://gitlab.com/carmelo0x63/micro-hugo-theme.git
```

2. change directory to `micro-hugo-theme/exampleSite`:
```
$ cd micro-hugo-theme/exampleSite
```

3. run `Hugo`:
```
$ hugo server --themesDir ../.. -D
```

### Create your own static site with theme = `Micro`
- A better approach may be to to create your own Hugo `site` with:
```
$ hugo new site myOwnSite && cd myOwnSite
```

- In there, initialize the site as a Git repository:
```
git init
```

- Then import the theme as a `git submodule`:
```
$ git submodule add https://gitlab.com/carmelo0x63/micro-hugo-theme themes/micro
```

- Customize it a bit by:
  - downloading a sample configuration file:
  ```
   $ curl -sSLfO https://gitlab.com/carmelo0x63/micro-hugo-theme/-/raw/main/exampleSite/hugo.toml
  ```
  - editing the configuration file to match your own settings:
  ```
   $ sed -i "s/micro-hugo-theme/micro/" hugo.toml
  ```
  **Hint**: on macOS GNU `gsed` should be used instead
  - generating some contents:
  ```
   $ hugo new about.md
   $ hugo new posts/post1.md
   $ hugo new snippets/snip1.md
  ```

- Finally, serve the site:
```
$ hugo server -D
```

### Update the theme
To update the theme run:
```
$ git submodule update --remote --merge
```

As an emergency procedure, for instance if you've manually modified the theme and merge isn't working, the following steps will remove then add the theme again:
```
$ git rm -f themes/micro

$ rm -rf .git/modules/themes/micro

$ git config --remove-section submodule.themes/micro
```

